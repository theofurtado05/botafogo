window.onload = () => {
        renderizaCards()
}

let container = document.querySelector('.container')
let filtro = document.getElementById('filtro-input')



function renderizaCards(){
    
    for (let jogador = 0; jogador <= jogadores.length; jogador++){
        container.innerHTML += `
            <div class="card" onclick='puxaJogador(${JSON.stringify(jogadores[jogador])})'>
                <img src="${jogadores[jogador]["imagem"]}">
                <h3>${jogadores[jogador]["nome"]}</h3>
                <h5>${jogadores[jogador]["posicao"]}</h5>                
            </div>
        `;
      
       
    }
}
 

function filtrar(){
    const inputFiltro = document.getElementById('filtro-input').value.toUpperCase()
    const cardsJogador = document.getElementsByClassName('card')

    for(let j = 0; j < cardsJogador.length; j++){
        let nomeJogador = cardsJogador[j].querySelector(".card h3")
        
        if(!(nomeJogador.innerText.toUpperCase().indexOf(inputFiltro) > -1)){
            cardsJogador[j].style.display = "none";
        }else{
            cardsJogador[j].style.display = ""
        }
    }
}


function puxaJogador(jogador){
    document.cookie =  jogador["nome"] + "#"
    document.cookie += jogador["posicao"] + "#"
    document.cookie += jogador["imagem"] + "#"
    document.cookie += jogador["descricao"] + "#"
    document.cookie += jogador["nome_completo"] + "#"
    document.cookie += jogador["nascimento"] + "#"
    document.cookie += jogador["altura_peso"]
    
    
    window.location.href = './details/details.html'

    document.cookie = ''

}




